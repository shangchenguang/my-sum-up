package com.shangcg.service;



/**
 * 售票服务，单纯的作为共享资源
 * 多线程锁产生的本质：多个线程操作同一个资源
 */
public interface SellTicket{

    /**
     * //TODO 多线程式测试服务不会停止，明白其原因
     * 单纯的卖票服务：带锁的卖票服务,线程安全
     * 1 多个线程同时卖，不用线程池查看结果 ： new Thread
     * 2 多个线程同时卖，用线程池，查看结果   new ThreadExecutors()
     * 3 多个线程同时卖票，线程池的方式，且指定线程池的名称  new ThreadExecutors()
     * 4 两个线程 轮流卖票，一个线程卖一张后下一个线程卖，比如A -> B 轮流交替 自旋 + lock + condition
     * 5 两个线程轮流卖票，用线程池方式，票卖完提示结束  自旋 + lock + condition +资源服务返回值
     * 6 10个线程轮流卖票，一个线程卖1张结束后第二线程开始卖，依此类推直到第十个线程结束
     */
    void sellWithLock();

    /**
     * 单纯的卖票服务:带返回值，方便外界判断
     *
     */
    Integer sellWithReturn();

    /**
     * 单纯的卖票服务:带返回值，方便外界判断
     *
     */
    boolean sellWithReturn1();

    /**
     * 带窗口信息的卖票
     */
    void sellWithLockAndThreadLocal();

    /**
     * 无锁卖票服务，利用原子操作类保证线程安全
     */
    void sellTicketWithAtomic();

    /**
     * 获取票的剩余数量，采用读锁，支持多个线程同时读
     * @return
     */
    Integer getSellNumber();

    /**
     * 卖票，采用写锁，同一时刻只支持一个线程进入
     */
    void sellTicketWithWriteLock();



}

package com.shangcg.service.impl;

import com.shangcg.service.SellTicket;
import com.shangcg.util.SellTicketUtilWithLock;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SellTicketImpl implements SellTicket {

    public SellTicketImpl(){

    }

    private Lock lock = new ReentrantLock();

    private Integer totalCount = 100;

    @Override
    public void sellWithLock(){
        lock.lock();
        try {
            if (totalCount <= 0){
                System.out.println(Thread.currentThread().getName() + " :票不足");
                return;
            }
            totalCount = totalCount -1;
            System.out.println("线程：" +Thread.currentThread().getName() + " 出票成功, 剩余：" + totalCount);
        }finally {
            lock.unlock();
        }

    }

    @Override
    public Integer sellWithReturn(){
        lock.lock();
        try {
            if (totalCount <= 0){
                System.out.println(Thread.currentThread().getName() + " :票不足");
                return 0;
            }
            totalCount = totalCount -1;
            System.out.println("线程：" +Thread.currentThread().getName() + " 出票成功, 剩余：" + totalCount);
        }finally {
            lock.unlock();
        }
        return 1;
    }

    @Override
    public boolean sellWithReturn1(){
        lock.lock();
        try {
            if (totalCount <= 0){
                System.out.println(Thread.currentThread().getName() + " :票不足");
                return false;
            }
            totalCount = totalCount -1;
            System.out.println("线程：" +Thread.currentThread().getName() + " 出票成功, 剩余：" + totalCount);
        }finally {
            lock.unlock();
        }
        return true;
    }

    @Override
    public void sellWithLockAndThreadLocal() {
        System.out.println(SellTicketUtilWithLock.threadLocal.get());
        sellWithLock();
    }

    private AtomicInteger ticketCount = new AtomicInteger(100);

    @Override
    public void sellTicketWithAtomic() {
        if (ticketCount.get() <= 0){
            System.out.println("票不足");
            return;
        }
        int stock = ticketCount.decrementAndGet();
        System.out.println(Thread.currentThread().getName() + ": 出票成功,剩余票数" + stock);
    }

    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    @Override
    public Integer getSellNumber() {
        readWriteLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "进行余票查询");
            System.out.println(totalCount);
            System.out.println(Thread.currentThread().getName() + "余票查询结束");
            return totalCount;
        }finally {
            readWriteLock.readLock().unlock();
        }
    }

    @Override
    public void sellTicketWithWriteLock() {
        readWriteLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() +"开始卖票");
            int stock = ticketCount.decrementAndGet();
            System.out.println(Thread.currentThread().getName() + ": 卖票结束,剩余票数" + stock);
        }finally {
            readWriteLock.writeLock().unlock();
        }
    }


}

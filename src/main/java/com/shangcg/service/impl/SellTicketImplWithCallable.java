package com.shangcg.service.impl;

import com.shangcg.service.CallableService;
import com.shangcg.service.SellTicket;

/**
 *  实现callable的卖票服务
 * @param <T>
 */
public class SellTicketImplWithCallable<T> implements CallableService<T> {

    @Override
    public T call() throws Exception {
        System.out.println(Thread.currentThread().getName() + "执行call");
        SellTicket sellTicket = new SellTicketImpl();
        return (T) sellTicket.sellWithReturn();
    }
}

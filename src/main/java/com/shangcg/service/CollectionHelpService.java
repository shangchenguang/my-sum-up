package com.shangcg.service;

/**
 * java容器类
 */
public interface CollectionHelpService {

    /**
     * 多线程并发修改arrayList 验证arrayList线程不安全抛异常
     */
    void threadUnSafeModifyList();

    /**
     * 线程安全的修改list 利用 copyOnWriteArrayList
     */
    void threadSafeModifyListWithCopyOnWriteArrayList();

    /**
     * 线程安全的修改list 利用 vector
     * copyOnWriteArrayList与vector区别：
     *
     */
    void threadSafeModifyListWithVector();


}

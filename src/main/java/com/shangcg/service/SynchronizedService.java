package com.shangcg.service;

public interface SynchronizedService {

    /**
     * 锁两个方法，多线程调用测试
     */
    void synchronizedTest();
}

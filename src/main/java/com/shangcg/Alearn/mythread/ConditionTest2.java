package com.shangcg.Alearn.mythread;

import java.util.concurrent.*;

/**

/**
 * 三个线程A、B、C A执行完后通知B、B执行完后通知C
 * A执行完调用B  B执行完调用C、 C执行完调用A
 * create by scg on 2021/9/29
 */
public class ConditionTest2 {



    public static void main(String[] args) {
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
                1,
                1,
                1000,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );

        poolExecutor.execute(() -> {

        });


    }
}



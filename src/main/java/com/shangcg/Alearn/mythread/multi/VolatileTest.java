package com.shangcg.Alearn.mythread.multi;

/**
 * volatile原理解析
 * 可见性的前提条件： 多个线程获取的是同一把锁
 * cpu相关术语：
 * 1内存屏障：一组处理器指令、用于实现对内存操作的顺序限制
 * 2缓冲行：
 * 3原子操作：
 * 4缓存行填充
 * 5缓存命中
 * 6写命中
 * 7写缺失：
 * @author shangchenguang
 * @date 2021/7/25 11:30 上午
 */
public class VolatileTest {
}

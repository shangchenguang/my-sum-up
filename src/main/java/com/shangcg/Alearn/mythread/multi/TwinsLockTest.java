package com.shangcg.Alearn.mythread.multi;

import org.junit.Test;

import java.util.concurrent.locks.Lock;

/**
 * @description:
 * @author: shangcg
 * @create: 2021-10-20 07:30
 */
public class TwinsLockTest {

    @Test
    public void test(){
        final Lock lock = new TwinsLock();
        class Worker extends Thread{
            public void run(){
                while (true){
                    lock.lock();
                    try {
                        ThreadState.SleepUtils.second(1);
                        System.out.println(Thread.currentThread().getName());
                        ThreadState.SleepUtils.second(1);
                    }finally {
                        lock.unlock();
                    }
                }
            }
        }

        for(int i = 0; i < 10; i++){
            Worker w = new Worker();
            w.setDaemon(true);
            w.start();
        }

        for (int i = 0; i< 10; i++){
            ThreadState.SleepUtils.second(1);
            System.out.println();
        }
    }
}
package com.shangcg.Alearn.mythread;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * @description: 一道题练习
 * lambda表达式() -> {}、 函数式编程 @FunctionInterface接口 、链式编程、 流式计算
 *
 * 现有5个用户需要筛选，只能用一行代码实现
 *
 * 1 ID必须是偶数
 * 2 年龄必须大于23岁
 * 3 用户名转为大写字母
 * 4 用户名字母倒着排序
 * 5 只能输出一个用户
 *
 * @author shangcg
 * @create: 2021-09-29 10:00
 */
public class StreamTest {

    public static void main(String[] args) {
        User user1 = new User(1, "a", 22);
        User user2 = new User(2, "b", 21);
        User user3 = new User(3, "c", 23);
        User user4 = new User(4, "d", 24);
        User user5 = new User(5, "e", 25);

        List<User> list = Arrays.asList(user1, user2, user3, user4, user5);

        //ID是偶数
        list.stream().filter((user) -> {return user.id % 2 == 0;});
    }
}

@Data
@AllArgsConstructor
class User{

    Integer id;
    String name;
    Integer age;
}


/*/**
 * new Predicate<User>() {
                    @Override
                    public boolean test(User user) {
                        return user.id % 2 == 0;
                    }
                }
   等价于 (User) -> {return user.id % 2 == 0}

 */

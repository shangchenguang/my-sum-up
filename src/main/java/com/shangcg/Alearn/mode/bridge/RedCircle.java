package com.shangcg.Alearn.mode.bridge;

/**
 * 桥接模式实现类1
 */
public class RedCircle implements DrawAPI{
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("Drawing circle : red"+ radius + x + y);
    }
}

package com.shangcg.Alearn.mode.decorator;

/**
 * 装饰器模式
 * 意图：动态的给一个对象添加一些额外的职责，比生成子类更灵活
 * 何时使用：在不想增加很多子类的情况下对类进行扩展
 * 优点：装饰类和被装饰类可以独立发展，不会互相耦合，是对继承模式的替代，动态扩展一个实现类的功能
 * 缺点：多层装饰的情况比较复杂
 *
 * 被装饰者接口
 */
public interface Shape {
    void draw();
}



package com.shangcg.Alearn.mode.adapter;

/**
 * 更高级的媒体播放器
 */
public interface AdvancedMediaPlayer {
    public void playVlc(String fileName);
    public void playMp4(String fileName);
}

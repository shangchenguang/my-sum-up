package com.shangcg.Alearn.io.rpc.server;

/**
 * rpc 服务提供者
 */
public interface HelloService {

    String sayHi(String name);
}

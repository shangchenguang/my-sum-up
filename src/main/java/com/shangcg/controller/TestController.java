package com.shangcg.controller;

import java.util.Vector;
import java.util.concurrent.atomic.AtomicLong;

import com.shangcg.entity.User;
import com.shangcg.service.IUserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {

	@Resource
	private IUserService userService;

	// http://localhost:8080/greeting?name=shangcg  访问测试路径  测试服务是否启动


	@RequestMapping("/greeting")
	public String greeting(@RequestParam(value="name", defaultValue="World") String name) {
		return "hello";
	}


	//http://localhost:8080/showUser?userId=1     测试mybatis是否连接正常


	@RequestMapping("/showUser")
	public User getUser(@RequestParam(value = "userId", defaultValue = "0") Integer userId) {
		try {
			User user = this.userService.getUserById(userId);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/insert_user", method = RequestMethod.POST)
	public void getUser() {
		try {
			userService.BatchInsertData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.shangcg.util.mvel;

import com.google.common.collect.Maps;

import java.util.Date;
import java.util.Map;
import org.mvel2.MVEL;

/**
 * mvel表达式引擎测试
 * Created by scg on 2021/11/8
 */
public class MvelUtils {

    public static void main(String[] args) {
        String expression = "a == null && b == nil ";
        Map<String,Object> map = Maps.newHashMap();
        map.put("a",null);
        map.put("b",null);

        Object object = MVEL.eval(expression, map);
        System.out.println(object);
    }

//    public static void main(String[] args) {
//        Date date = new Date();
//        System.out.println(date);
//    }




}
